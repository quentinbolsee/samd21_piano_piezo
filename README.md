# SAMD21E piano and synthesizer

This project demonstrates capacitive touch included in the SAMD21E, as well as a realtime synthesizer running at 44.1kHz in an interrupt.

<img src=video/synthesizer.mp4 width=50%><br>

A total of 5 capacitive pads are available, and the sound is produced by a piezo buzzer on the back:

<img src=img/front.jpg width=50%><img src=img/back.jpg width=50%><br>

## Design

### Schematic

The schematic is the bare minimal to get the SAMD21E to work. Note that I put a pull-up resistor and decoupling capacitor on the RESET pin, as advised in the datasheet. An RGB LED is a great device for user feedback, I made sure to connect it to 3 PWM-capable pins.

[![](img/schematic.png)](img/schematic.png)

The piezo buzzer is connected to the only DAC-capable pin of the D21E, namely PA02.

### Board layout

The layout is straightforward, but I wanted an organic connection to the pads, so I left them out in the design then completed them in inkscape.

[![](img/board.png)](img/board.png)

After exporting the .svg files of the board, I added the missing traces as bezier splines:

[![](img/inkscape.png)](img/inkscape.png)

## Making

### Milling

The milling was performed in around 30 minutes with the following operations:

- Two isolation passes with a 0.4mm flat end mill
- A copper clearing pass with a 0.8mm flat end mill
- Edge cut and drills with the 0.8mm mill

[![](img/milling.jpg)](img/milling.jpg)

After a cleanup with soap, the copper shines and the clarity of the traces becomes clear. Note the difference height between 0.4mm and 0.8mm mill passes, that is not wanted but hard to calibrate right.

[![](img/milling_done.jpg)](img/milling_done.jpg)

### Soldering

Before soldering, I always look at my boards with a backlight. This makes potential shorts very obvious. In this case, I'm satisfied with the contrast between traces and clear regions:

[![](img/microscope.jpg)](img/microscope.jpg)

I proceed to soldering the components. I add a generous amount of flux in tight pitch areas such as the SAMD21's legs.

[![](img/soldering.jpg)](img/soldering.jpg)

A microscope always helps to check for solder bridges.

[![](img/microscope_view.jpg)](img/microscope_view.jpg)

The soldering is complete:

[![](img/front.jpg)](img/front.jpg)

The back shows the piezo and a shim for the USB plug

[![](img/back.jpg)](img/back.jpg)

## Code

Before programming the board through Arduino IDE, I flash the SAM BA bootloader using a SWD programmer and edbg. Note the connection: the dot on the 2x2 plug is on the same orientation on both the programmer and target:

[![](img/programming.jpg)](img/programming.jpg)

The bootloaders can be found in the [Fab SAM Arduino Core](https://github.com/qbolsee/ArduinoCore-fab-sam) at [this location](https://github.com/qbolsee/ArduinoCore-fab-sam/tree/master/bootloaders/zero/binaries). For a SAMD21E17, the edbg command is:

```
edbg -ebpv -t samd21 -f sam_ba_SAMD21E17A.bin
```

After installing the Arduino core, the board can be flashed through Arduino IDE.

### Adafruit Freetouch

The code makes intensive use of the Adafruit Freetouch library, which is a clever reverse engineering of the Qtouch hardware included in the SAMD family. The library must be installed separately, you can find more information [here](https://www.arduino.cc/reference/en/libraries/adafruit-freetouch-library/).

### Synthesizer

The synthesizer is a simple low-passed square wave, with attack and release effects. This is all computed within an interrupt running at 44.1kHz, producing 10-bit samples sent to the DAC.

## Demo

<img src=video/synthesizer.mp4 width=50%><br>

The piezo signal shows the attack/release of the synthesizer:

<img src=video/scope1.mp4 width=50%><br>

Zoomed out, the linearity of the attack/release is obvious, note how the release is 3 times longer on purpose:

<img src=video/scope2.mp4 width=50%><br>

## Downloads

### PNG files (1000 DPI)

Traces:

<img src=design/png/D21E_piano-F_Cu.png width=50%><br>

Interior:

<img src=design/png/D21E_piano-interior_drill_comp.png width=50%><br>

Traces + exterior:

<img src=design/png/D21E_piano-interior_traces_comp.png width=50%><br>

Composite:

<img src=design/png/D21E_piano-composite.png width=50%><br>

### KiCAD 6 files

- [D21E_piano.zip](design/D21E_piano.zip)

### Code

Full synthesizer:
- [d21_synth.zip](code/d21_synth.zip)

Simplified version, square wave only:
- [d21_square_wave.zip](code/d21_square_wave.zip)

## License

This project is provided under the MIT License.
